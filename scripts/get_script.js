// id corpo_texto

console.clear();
var links = document.querySelectorAll('table table table')[2].querySelectorAll('tr > td > a');
var linksLength = links.length;

var result = {};

function get(i) {
    console.log(i, 'de', linksLength);
    if (i < linksLength) {

        if (links[i] !== undefined && links[i].innerText.indexOf('CEP') > 0) {
            result[i] = {};

            var cep = links[i].innerText.substr(links[i].innerText.indexOf('CEP:') + 4, 9);
            result[i]['cep'] = cep;

            var split = links[i].innerText.substr(0, links[i].innerText.indexOf('CEP:')).split('-');
            result[i]['address'] = split[0].trim();
            result[i]['type'] = split[1].trim();

            if (links[i].href !== undefined && links[i].href !== null && links[i].href.trim() !== '') {
                var xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function () {

                    if (xhttp.readyState == 4 && xhttp.status == 200) {

                        var parser = new DOMParser();
                        var element = parser.parseFromString(xhttp.responseText, 'text/html').querySelector('#corpo_texto');

                        var text = (element !== undefined && element !== null) ? element.innerText : null;

                        result[i]['text'] = text;

                        get(++i);
                    } else if (xhttp.readyState == 4) {

                        result[i]['fail'] = true;
                    }
                };

                xhttp.open("GET", links[i].href, true);
                xhttp.send();
            }
        } else {

            get(++i);
        }
    } else {

        console.log(result);
        console.log(JSON.stringify(result));
    }
}
get(0);
