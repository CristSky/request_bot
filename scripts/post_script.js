// ETAPA 1 - jquery injection
var script = document.createElement('script');
script.src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js";
document.getElementsByTagName('head')[0].appendChild(script);

var form = document.getElementsByTagName('form')[0];
form.onsubmit = function () {
    return false;
};

var tipoOptions = document.querySelectorAll('#tipo_id_tipo > option');
var bairrosOptions = document.querySelectorAll('#bairros_id_bairros > option');

// ETAPA 2 - criar var jdata com o json como o modelo
var jdata = [
    {
        "cep": "17504-031",
        "logradouro": "Alto Cafezal",
        "historia": ""
    },
    {
        "cep": "17504-013",
        "logradouro": "Cinco de Julho",
        "historia": ""
    },
    {
        "cep": "17504-075",
        "logradouro": "Praca da Biblia",
        "historia": ""
    }
];


var interval = setInterval(function () {

    if (jdata.length > 0) {
        var data = jdata.shift();

        document.getElementById('tipo_id_tipo').value = data.tipo;//tipoOptions[3].value;
        document.querySelector('[name=ruas_nome]').value = data.logradouro;
        document.getElementById('bairros_id_bairros').value = data.bairro;//bairrosOptions[5].value;
        document.getElementById('ruas_sobre').value = data.historia;
        document.querySelector('[name=ruas_cep]').value = data.cep;

        $.ajax({
            type: 'POST',
            url: form.action,
            data: $(form).serialize(),
            dataType: "html"
        }).done(function (a) {
            console.log('Enviado com sucesso');
        });
    }
}, 500);


var t = document.querySelectorAll('#teste a[href*="index.php?area=rua&codigo="]');
var ar = [];
for (var i = 0; i < t.length; i++) {
    ar.push(t[i].innerText);
}

var newJdata = [];
for (var i = 0; i < ar.length; i++) {
    for (var j = jdata.length - 1; j >= 0; j--) {
        if (jdata[j] !== undefined && ar[i] == jdata[j].logradouro) {
            newJdata.push(jdata.splice((j, 1)));
        }
    }
}

for (var i = 0; i < newJdata.length; i++) {
    for (var j = jdata.length - 1; j >= 0; j--) {
        if (jdata[j].logradouro == newJdata[j].logradouro) {
            delete jdata[j];
        }
    }
}

var newJdata = [];
for (var i = 0; i < jdata.length; i++) {
    if (jdata[i] != undefined) {
        newJdata.push((jdata[i]));
    }
}
