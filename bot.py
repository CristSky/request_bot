__author__ = 'cristiano'
## robo python com bypass request via Tor Proxy ##

import csv
import urllib
import json
from urllib.request import Request, urlopen

##################################################################
## pip install pysocks ##
import socks
import smtplib

## Tor Proxy Connection ##
socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5, '127.0.0.1', 9050)
socks.wrapmodule(smtplib)
##################################################################

file = 'data_i.json'
j_obj = open(file).read()
dec = json.loads(j_obj)
size = len(dec)

user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36'

with open('lista.csv', 'a+', newline='') as csvfile:
    fieldnames = ['cep', 'address', 'bairro', 'type', 'text']
    writer = csv.DictWriter(csvfile, delimiter=';', fieldnames=fieldnames)

    writer.writeheader()

    for i in range(size):
        if dec[str(i)]['text']:
            dec[str(i)]['text'] = dec[str(i)]['text'].strip()
        cep = dec[str(i)]['cep']
        print(i, cep)
        url_json = 'https://viacep.com.br/ws/' + cep + '/json/'
        url = Request(url_json, headers={'User-Agent': user_agent})
        r = urllib.request.urlopen(url)
        data = json.loads(r.read().decode(r.info().get_param('charset') or 'utf-8'))
        try:
            dec[str(i)]['bairro'] = data['bairro']
            print(i, 'of', size, data['bairro'])
        except:
            dec[str(i)]['bairro'] = ""
            print(i, 'of', size, cep, 'None')
        writer.writerow(dec[str(i)])

print('DONE!')
